//
//  AppDelegate.swift
//  MarvelExam
//
//  Created by Cristian Cañedo on 16/08/18.
//  Copyright © 2018 Cristian Cañedo. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let reqVid = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
        reqVid.returnsObjectsAsFaults = false
        
        do{
            let res = try self.persistentContainer.viewContext.fetch(reqVid)
            
            if res.count == 0{
                self.getHeros()
            }
            
        }catch{}
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "MarvelExam")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func getHeros(){
        print("appDelegate")
        Alamofire.request(JSONKeys.URL_GET_CHARACTERS).responseJSON{res in
            
            if let JSON = res.result.value as? NSDictionary{
                
                let data = JSON.value(forKey: JSONKeys.KEY_DATA) as? NSDictionary ?? [:]
                let results = data.value(forKey: JSONKeys.KEY_RESULTS) as? NSArray ?? []
                let context = self.persistentContainer.viewContext
                print(JSON)
                
                
                for i in results{
                    let heroObject = i as? NSDictionary
                    
                    let name = heroObject?.value(forKey: JSONKeys.KEY_NAME) as? String ?? ""
                    let date = heroObject?.value(forKey: JSONKeys.KEY_MODIFIED) as? String ?? ""
                    let description = heroObject?.value(forKey: JSONKeys.KEY_DESCRIPTION) as? String ?? ""
                    let idHero = heroObject?.value(forKey: JSONKeys.KEY_ID) as? Int ?? -1
                    let comicsObj = heroObject?.value(forKey: JSONKeys.KEY_COMICS) as? NSDictionary ?? [:]
                    let arrayComics = comicsObj.value(forKey: JSONKeys.KEY_ITEMS) as? NSArray ?? []
                    
                    let imageObject = heroObject?.value(forKey: JSONKeys.KEY_THUMBNAIL) as? NSDictionary ?? [:]
                    let imgPath = imageObject.value(forKey: JSONKeys.KEY_PATH) as? String ?? ""
                    let imgExtension = imageObject.value(forKey: JSONKeys.KEY_EXTENSION) as? String ?? ""
                    
                    
                    
                    
                    let req = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
                    req.returnsObjectsAsFaults = false
                    
                    do{
                        let res = try self.persistentContainer.viewContext.fetch(req)
                        
                        if res.count > 0{
                            
                            var ban = 0
                            for i in res as! [NSManagedObject]{
                                
                                if let id = i.value(forKey: "id") as? Int{
                                    
                                    if id == idHero{
                                        ban += 1
                                    }
                                }
                            }
                            
                            if ban == 0{
                                
                                let newComuni = NSEntityDescription.insertNewObject(forEntityName: "Heros", into: context)
                                newComuni.setValue(name , forKey: "name")
                                newComuni.setValue(date , forKey: "date")
                                newComuni.setValue(description , forKey: "descriptionHero")
                                newComuni.setValue(idHero, forKey: "id")
                                newComuni.setValue(imgPath , forKey: "image")
                                newComuni.setValue(imgExtension , forKey: "extensionImage")
                                
                                do{
                                    
                                    try self.persistentContainer.viewContext.save()
                                    
                                    for j in arrayComics{
                                        let jObj = j as? NSDictionary
                                        
                                        let name = jObj?.value(forKey: JSONKeys.KEY_NAME) as? String ?? ""
                                        let res = jObj?.value(forKey: JSONKeys.KEY_RESOURSE_URI) as? String ?? ""
                                        
                                        let newCom = NSEntityDescription.insertNewObject(forEntityName: "Comics", into: context)
                                        newCom.setValue(name , forKey: "name")
                                        newCom.setValue(res , forKey: "resourse")
                                        newCom.setValue(idHero , forKey: "idHero")
                                        
                                        do{
                                            
                                            try self.persistentContainer.viewContext.save()
                                            
                    
                                        }catch{
                                            
                                        }
                                    }
                                    
                                }catch{
                                    
                                }
                            }
                            
                        }else{
                            
                            let newComuni = NSEntityDescription.insertNewObject(forEntityName: "Heros", into: context)
                            newComuni.setValue(name , forKey: "name")
                            newComuni.setValue(date , forKey: "date")
                            newComuni.setValue(description , forKey: "descriptionHero")
                            newComuni.setValue(idHero, forKey: "id")
                            newComuni.setValue(imgPath , forKey: "image")
                            newComuni.setValue(imgExtension , forKey: "extensionImage")
                            
                            do{
                                
                                try self.persistentContainer.viewContext.save()
                                
                            }catch{
                                
                            }
                            
                        }
                        
                    }catch{}
                    
                }
                
            }
            
        }
        
    }

}

