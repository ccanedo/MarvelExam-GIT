//
//  ViewController.swift
//  MarvelExam
//
//  Created by Cristian Cañedo on 16/08/18.
//  Copyright © 2018 Cristian Cañedo. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var tvHome: UITableView!
    
    var arrayHeros:[heroObject] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDatabaseHeros()
    }
    
 
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func getHeros(){
        print("ViewControoler")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        Alamofire.request(JSONKeys.URL_GET_CHARACTERS).responseJSON{res in
            
            if let JSON = res.result.value as? NSDictionary{
                
                let data = JSON.value(forKey: JSONKeys.KEY_DATA) as? NSDictionary ?? [:]
                let results = data.value(forKey: JSONKeys.KEY_RESULTS) as? NSArray ?? []
                
                
                for i in results{
                    let heroObj = i as? NSDictionary
                    
                    let name = heroObj?.value(forKey: JSONKeys.KEY_NAME) as? String ?? ""
                    let date = heroObj?.value(forKey: JSONKeys.KEY_MODIFIED) as? String ?? ""
                    let description = heroObj?.value(forKey: JSONKeys.KEY_DESCRIPTION) as? String ?? ""
                    let idHero = heroObj?.value(forKey: JSONKeys.KEY_ID) as? Int ?? -1
                    
                    let imageObject = heroObj?.value(forKey: JSONKeys.KEY_THUMBNAIL) as? NSDictionary ?? [:]
                    let imgPath = imageObject.value(forKey: JSONKeys.KEY_PATH) as? String ?? ""
                    let imgExtension = imageObject.value(forKey: JSONKeys.KEY_EXTENSION) as? String ?? ""
                    
                    
                    let obj = heroObject()
                    obj.description = description
                    obj.date = date
                    obj.extensionImage = imgExtension
                    obj.id = idHero
                    obj.image = imgPath
                    obj.name = name
                    self.arrayHeros.append(obj)
                    
                    let req = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
                    req.returnsObjectsAsFaults = false
                    
                    do{
                        let res = try context.fetch(req)
                        
                        if res.count > 0{
                            
                            var ban = 0
                            for i in res as! [NSManagedObject]{
                                
                                if let id = i.value(forKey: "id") as? Int{
                                    
                                    if id == idHero{
                                        ban += 1
                                    }
                                }
                            }
                            
                            if ban == 0{
                                
                                let newComuni = NSEntityDescription.insertNewObject(forEntityName: "Heros", into: context)
                                newComuni.setValue(name , forKey: "name")
                                newComuni.setValue(date , forKey: "date")
                                newComuni.setValue(description , forKey: "descriptionHero")
                                newComuni.setValue(idHero, forKey: "id")
                                newComuni.setValue(imgPath , forKey: "image")
                                newComuni.setValue(imgExtension , forKey: "extensionImage")
                                
                                do{
                                    
                                    try context.save()
                                    
                                }catch{
                                    
                                }
                            }
                            
                        }else{
                            
                            print("DOS")
                            
                        }
                        
                    }catch{}
                    
                }
                self.tvHome.reloadData()
            }
            
        }
        
    }
    
    func getDatabaseHeros() {
        arrayHeros.removeAll()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let reqCom = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
        reqCom.returnsObjectsAsFaults = false
        
        do{
            let res = try context.fetch(reqCom)
            
            if res.count > 0{
                
                for i in res as! [NSManagedObject]{
                    print(i.value(forKey: "name") as! String)
                    let obj = heroObject()
                    obj.description = i.value(forKey: "descriptionHero") as! String
                    obj.date = i.value(forKey: "date") as! String
                    obj.extensionImage = i.value(forKey: "extensionImage") as! String
                    obj.id = i.value(forKey: "id") as! Int
                    obj.image = i.value(forKey: "image") as! String
                    obj.name = i.value(forKey: "name") as! String
                    arrayHeros.append(obj)
                    
                }
                
                tvHome.reloadData()
                
            }else{
                
                getHeros()
            }
            
        }catch{
            print("NEL")
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayHeros.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeros", for: indexPath) as! CustomTableViewCell
        
        let resource = ImageResource(downloadURL: URL(string: "\(arrayHeros[indexPath.row].image).\(arrayHeros[indexPath.row].extensionImage)")!, cacheKey: "\(arrayHeros[indexPath.row].image).\(arrayHeros[indexPath.row].extensionImage)")
        cell.imgHero.kf.setImage(with: resource)
        
        cell.nameHero.text = arrayHeros[indexPath.row].name
        cell.dateModifyHero.text = arrayHeros[indexPath.row].date
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "detailController") as! DetailsViewController
        vc.idHero = arrayHeros[indexPath.row].id
        vc.delete = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension ViewController: deleteDelegate{
    func delete() {
        print("jhvbgvhg")
        getDatabaseHeros()
    }
    
    
}
