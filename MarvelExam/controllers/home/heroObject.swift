//
//  heroObject.swift
//  MarvelExam
//
//  Created by Cristian Cañedo on 16/08/18.
//  Copyright © 2018 Cristian Cañedo. All rights reserved.
//

import Foundation

class heroObject{
    
    var name = ""
    var image = ""
    var date = ""
    var extensionImage = ""
    var id = -1
    var description = ""
}
