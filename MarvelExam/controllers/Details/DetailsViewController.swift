//
//  DetailsViewController.swift
//  MarvelExam
//
//  Created by Cristian Cañedo on 16/08/18.
//  Copyright © 2018 Cristian Cañedo. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var viewBackDescription: UIView!
    @IBOutlet weak var lblNameHero: UILabel!
    @IBOutlet weak var lblDateModified: UILabel!
    @IBOutlet weak var imgHero: UIImageView!
    @IBOutlet weak var lblDescriptionHero: UILabel!
    @IBOutlet weak var lblIdHero: UILabel!
    
    @IBOutlet weak var imgEditImage: UIImageView!
    @IBOutlet weak var txtNameEdit: UITextField!
    @IBOutlet weak var txtDescriptionEdit: UITextField!
    @IBOutlet weak var viewBodyAlert: UIView!
    @IBOutlet weak var viewBlurAlert: UIView!
    
    @IBOutlet weak var tvComics: UITableView!
    var delete:deleteDelegate?
    var idHero = -1
    var arrayComics:[String] = []
//    PUTOOOOO
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBackDescription.layer.cornerRadius = 10.0
        viewBodyAlert.layer.cornerRadius = 5.0
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        
        let reqCom = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
        reqCom.returnsObjectsAsFaults = false
        
        do{
            let res = try context.fetch(reqCom)
            
            if res.count > 0{
                
                for i in res as! [NSManagedObject]{
                    
                    if i.value(forKey: "id") as! Int == idHero{
                        
                        let res = ImageResource(downloadURL: URL(string: "\(i.value(forKey: "image") as! String).\(i.value(forKey: "extensionImage") as! String)")! , cacheKey: "\(i.value(forKey: "image") as! String).\(i.value(forKey: "extensionImage") as! String)")
                        
                        lblDescriptionHero.text = i.value(forKey: "descriptionHero") as? String ?? ""
                        lblDateModified.text = i.value(forKey: "date") as? String ?? ""
                        imgHero.kf.setImage(with: res)
                        lblNameHero.text = i.value(forKey: "name") as? String ?? ""
                        lblIdHero.text = "\(i.value(forKey: "id") as? Int ?? 0)"
                    }
                    
                }
                
            }
            
        }catch{
            print("NEL")
        }
        
        
        let reqComic = NSFetchRequest<NSFetchRequestResult>(entityName: "Comics")
        reqComic.returnsObjectsAsFaults = false
        
        do{
            let res = try context.fetch(reqComic)
            
            if res.count > 0{
                
                for i in res as! [NSManagedObject]{
                    
                    if i.value(forKey: "idHero") as! Int == idHero{
                        
                        arrayComics.append(i.value(forKey: "name") as! String)
                        
                    }
                    
                }
                
                tvComics.reloadData()
                
            }
            
        }catch{
            print("NEL")
        }
        
    }
    
    
    @IBAction func actionEdit(_ sender: UIButton) {
        
        
        self.viewBlurAlert.isHidden = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let req = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
        req.returnsObjectsAsFaults = false
        
        
        let name = txtNameEdit.text!
        let desc = txtDescriptionEdit.text!
        let date = Date()
        
        
        
        do{
            let res = try context.fetch(req)

            for i in res as! [NSManagedObject]{

                if let id = i.value(forKey: "id") as? Int{

                    if id == idHero{


                        i.setValue("\(date)" , forKey: "date")
                        i.setValue(name , forKey: "name")
                        i.setValue(desc , forKey: "descriptionHero")
                        i.setValue(i.value(forKey: "image" ) as! String, forKey: "image")
                        i.setValue( i.value(forKey: "extensionImage") as! String, forKey: "extensionImage")

                        do{
                            
                            try context.save()
                            lblNameHero.text = name
                            lblDescriptionHero.text = desc
                            lblDateModified.text = "\(date)"
                            delete?.delete()
                        }catch{

                        }

                    }
                }
            }

        }catch{}
        
    }
    
    @IBAction func actionShowMore(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Mas opciones", message: "Selecciona una opción", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Editar", style: .default , handler:{ (UIAlertAction)in
            
            self.viewBlurAlert.isHidden = false
            self.imgEditImage.image = self.imgHero.image
            self.txtNameEdit.text = self.lblNameHero.text!
            self.txtDescriptionEdit.text = self.lblDescriptionHero.text!
            
        }))
        
        alert.addAction(UIAlertAction(title: "Eliminar", style: .destructive , handler:{ (UIAlertAction)in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let req = NSFetchRequest<NSFetchRequestResult>(entityName: "Heros")
            req.returnsObjectsAsFaults = false
            
            
            
            do{
                let res = try context.fetch(req)
                
                for i in res as! [NSManagedObject]{
                    
                    if let id = i.value(forKey: "id") as? Int{
                        
                        if id == self.idHero{
                            
                            
                            context.delete(i)
                            
                            do{
                                try context.save()
                                self.delete?.delete()
                                self.navigationController?.popViewController(animated: true)
                                
                            }catch{
                                
                            }
                            
                        }
                    }
                }
                
            }catch{}
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    
}
extension DetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayComics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellComics", for: indexPath)
        cell.textLabel?.text = arrayComics[indexPath.row]
        return cell
    }
    
}
protocol deleteDelegate {
    func delete()
}
