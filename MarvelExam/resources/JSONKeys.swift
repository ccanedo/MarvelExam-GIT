//
//  JSONKeys.swift
//  MarvelExam
//
//  Created by Cristian Cañedo on 16/08/18.
//  Copyright © 2018 Cristian Cañedo. All rights reserved.
//

import Foundation

class JSONKeys{
    
    static let URL_GET_CHARACTERS = "http://gateway.marvel.com/v1/public/characters?apikey=7e4845ee10087cbd72d1aea7b0f9ab39&ts=1534474931055&hash=13db978cacd4b08291195eb68f6c4e86"
    
    
    static let KEY_DATA = "data"
    static let KEY_RESULTS = "results"
    static let KEY_ID = "id"
    static let KEY_NAME = "name"
    static let KEY_DESCRIPTION = "description"
    static let KEY_MODIFIED = "modified"
    static let KEY_THUMBNAIL = "thumbnail"
    static let KEY_PATH = "path"
    static let KEY_EXTENSION = "extension"
    static let KEY_COMICS = "comics"
    static let KEY_ITEMS = "items"
    static let KEY_RESOURSE_URI = "resourceURI"
}
