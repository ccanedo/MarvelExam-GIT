//
//  CustomTableViewCell.swift
//  MarvelExam
//
//  Created by Cristian Cañedo on 16/08/18.
//  Copyright © 2018 Cristian Cañedo. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var imgHero: UIImageView!
    @IBOutlet weak var nameHero: UILabel!
    @IBOutlet weak var dateModifyHero: UILabel!
    
}
